<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class SetupDevEnvironment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:setup
                         {--with-new-user : Create new user}
                         {--env= : The environment to use}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected      $description = 'Set up the development environment';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // Display a starting message
        $this->info('Starting setting up application environment');

        // Check and validate the specified environment or use the default environment
        $this->checkEnv();

        // Run database migrations and seed initial data
        $this->migrateAndSeedDatabase();

        // Create a fake test user for demonstration purposes
        if ($this->option('with-new-user')) {
            $this->createFakeUser();
        }

        // Display a completion message
        $this->info('All done . Bye!');
    }


    /**
     * Migrates the database schema and seeds initial data.
     */
    private function migrateAndSeedDatabase(): void
    {
        // Get the current environment from the app configuration
        $environment = config('app.env');

        try {
            // Test database connection
            DB::connection()->getPdo();

            // Run the 'migrate:fresh' Artisan command to reset and migrate the database schema
            // This will drop all existing tables and then recreate them based on migration files
            $this->call('migrate:fresh', ['--env' => $environment]);

            // Run the 'db:seed' Artisan command to seed the database with initial data
            // Seeders provide a way to populate database tables with sample data
            $this->call('db:seed', ['--env' => $environment]);

        } catch (\Exception $e) {
            // Check if the database exists
            $dbDatabase = DB::connection()->getDatabaseName();

            // If the database doesn't exist, display an error
            $this->error("The database '{$dbDatabase}' does not exist for the '{$environment}' environment.");
            die();
        }
    }

    /**
     * Create a fake test user for demonstration purposes.
     * This function uses Laravel's model factory to generate a user with random attributes.
     * It displays user information such as name, email, and a default password.
     */
    private function createFakeUser(): void
    {
        $this->info('Creating test user');

        // Generate a fake user using the User model factory
        $user = User::factory()->create();

        // Display user information
        $this->info($user->first_name . ' ' . $user->last_name . ' created');
        $this->warn('Email: ' . $user->email);
        $this->warn('Password: password');

    }
    /**
     * Check the validity of the specified environment or use the default environment.
     * If the specified environment is not found, display an error message and exit.
     * Set the desired environment if it's valid.
     */
    private function checkEnv(): void
    {
        // Check if the specified environment exists, otherwise use the default environment
        $desiredEnvironment = $this->option('env') ?? config('app.env');

        if (!App::environment($desiredEnvironment)) {
            // Display an error message if the specified environment is not valid
            $this->error("The specified environment '{$desiredEnvironment}' does not exist.");
            die();
        } else {
            // Set the desired environment for the upcoming tasks
            App::environment($desiredEnvironment);
        }
    }
}


