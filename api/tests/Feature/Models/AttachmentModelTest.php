<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\{
    Attachment,
    Task,
    User
};

beforeEach(function () {
    // Create a user
    $this->user = User::create([
                                   'name'     => 'John Doe',
                                   'email'    => 'john@example.com',
                                   'password' => bcrypt('password'),
                               ]);

    // Create a task
    $this->task = Task::create([
                                   'user_id'     => $this->user->id,
                                   'title'       => 'Test Task',
                                   'description' => 'This is a test task',
                               ]);
});

// Test the case where an attachment can be created successfully.
it('can create an attachment', function () {
    // Create new attachment
    $attachment = Attachment::create([
                                 'task_id'   => $this->task->id,
                                 'file_path' => 'path/to/file'
                             ]);

    // Then
    expect($attachment)
        ->toBeInstanceOf(Attachment::class)
        ->and($attachment->file_path)
        ->toEqual('path/to/file')
        ->and($attachment->task_id)
        ->toEqual($this->task->id);
});

// Test case for verifies that an attachment can be successfully retrieved from the database
it('can read an attachment', function () {
    // Create new attachment
    $attachment = Attachment::create([
                                 'task_id'   => $this->task->id,
                                 'file_path' => 'path/to/file'
                             ]);

    // Retrieve the attachment from model using its ID
    $found = Attachment::find($attachment->id);

    // Assert the attachment should not be null
    // and the path is the expected
    expect($found)
        ->not()
        ->toBeNull()
        ->and($found->file_path)
        ->toEqual('path/to/file');
});

// Test the case where an attachment can be updated successfully.
it('can update an attachment', function () {
    // Create new attachment
    $attachment = Attachment::create([
                                 'task_id'   => $this->task->id,
                                 'file_path' => 'path/to/file'
                             ]);

    // Define a new path string
    $newFilePath = 'path/to/updated/file';

    // Updating the attachment
    $attachment->update(['file_path' => $newFilePath]);

    // Assertions
    $this->assertDatabaseHas('attachments', [
        'id'        => $attachment->id,
        'file_path' => $newFilePath,
    ]);
});

// Test the case where an attachment can be deleted successfully.
it('can delete an attachment', function () {
    // Create new attachment
    $attachment = Attachment::create([
                                 'task_id'   => $this->task->id,
                                 'file_path' => 'path/to/file'
                             ]);

    // Deleting the attachment
    $attachment->delete();

    // Assert isn't in database
    $this->assertDatabaseMissing('attachments', [
        'id' => $attachment->id,
    ]);
});

// Test the case where the task associated with an attachment can be retrieved.
it('can get the task associated with an attachment', function () {
    // Create new attachment
    $attachment = Attachment::create([
                                         'task_id'   => $this->task->id,
                                         'file_path' => 'path/to/file'
                                     ]);

    // Assertions
    expect($attachment->task->is($this->task))->toBeTrue();
});
