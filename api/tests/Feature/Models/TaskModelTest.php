<?php

use App\Models\{
    Attachment,
    Task,
    User
};

beforeEach(function () {
    // Create a user
    $this->user = User::create([
           'name'     => 'John Doe',
           'email'    => 'john@example.com',
           'password' => bcrypt('password'),
       ]);

    // Create a task
    $this->task = Task::create([
           'user_id' => $this->user->id,
           'title' => 'Test Task',
           'description' => 'This is a test task',
       ]);

    // Create a attachment
    $this->attachment = Attachment::create([
           'task_id' => $this->task->id,
           'file_path' => 'path/to/file',
       ]);
});

// Test the case where a task can be created successfully.
it('can create a task', function () {
    // Given
    $taskData = [
        'user_id' => $this->user->id,
        'title' => 'Test Task',
        'description' => 'This is a test task',
    ];

    // When
    $task = Task::create($taskData);

    // Then
    expect($task)
        ->toBeInstanceOf(Task::class)
        ->and($task->title)
        ->toBe('Test Task')
        ->and($task->description)
        ->toBe('This is a test task')
        ->and($task->user_id)
        ->toBe($this->user->id)
        ->and($task->is_completed)
        ->toBeFalse();
});

// Test the case where a task can be updated successfully.
it('can update a task', function () {
    // Given
    $task = Task::create([
                             'user_id' => $this->user->id,
                             'title' => 'Old Title',
                             'description' => 'This is an old title'
                         ]);

    // When
    $task->update(['title' => 'New Title']);

    // Then
    expect($task->refresh()->title)->toBe('New Title');
});

// Test the case where a task can be marked as completed.
it('can mark task as completed', function () {
    // Given
    $task = Task::create([
                             'user_id' => $this->user->id,
                             'title' => 'Another Test Task',
                             'description' => 'This is another test task',
                         ]);

    // When
    $task->update(['completed_at' => now()]);

    // Then
    expect($task->refresh()->is_completed)->toBeTrue();
});

// Test the case where a task can be deleted successfully.
it('can delete a task', function () {
    // Given
    $task = Task::create([
                             'user_id' => $this->user->id,
                             'title' => 'Test Task',
                             'description' => 'This is a test task'
                         ]);

    // When
    $task->delete();

    // Then
    expect(Task::find($task->id))
        ->toBeNull()
        ->and(Task::onlyTrashed()
                  ->find($task->id))
        ->not()
        ->toBeNull();
});

// Test the case where a user can have more than one task
it('a user can have multiple tasks', function () {
    // Given another task
    $anotherTask = Task::create([
            'user_id' => $this->user->id,
            'title' => 'Another Task',
            'description' => 'This is another task',
        ]);

    // Then
    expect($this->user->tasks)
        ->toHaveCount(2)
        ->and($this->user->tasks->pluck('id'))
        ->toContain($this->task->id)
        ->and($this->user->tasks->pluck('id'))
        ->toContain($anotherTask->id);
});

// Test case to ensure that a task is associated with a user
it('a task belongs to a user', function () {
    // Then
    expect($this->task->user->is($this->user))->toBeTrue();
});

// Test the case where a task's attachments can be retrieved.
it('can get task attachments', function () {
    $task = Task::create([
                             'user_id'     => $this->user->id,
                             'title'       => 'Task with attachments',
                             'description' => 'This is another task',
                         ]);
    // Given: Create an attachment associated with a task
    Attachment::create([
                   'task_id'   => $task->id,
                   'file_path' => 'path/to/file',
               ]);

    // When: Retrieve the attachments for the task
    $retrievedAttachments = $task->attachments;

    // Then: Verify that the retrieved attachments include the one we just created
    expect($retrievedAttachments)
        ->toHaveCount(1)
        ->and($retrievedAttachments->first()->file_path)
        ->toEqual('path/to/file');

});

it('a task can have multiple attachments', function () {
    // Given another attachment
    $anotherAttachment = Attachment::create([
                                        'task_id' => $this->task->id,
                                        'file_path' => 'path/to/another/file',
                                    ]);

    // Then
    expect($this->task->attachments)
        ->toHaveCount(2)
        ->and($this->task->attachments->pluck('id'))
        ->toContain($this->attachment->id)
        ->and($this->task->attachments->pluck('id'))
        ->toContain($anotherAttachment->id);
});

// Test if an exception is trowing when no user assign to the task
it('throws an exception if trying to create a task without a user', function () {
    // Expect
    $this->expectException(\Illuminate\Database\QueryException::class);

    // When
    Task::create([
                     'title' => 'Test Task',
                     'description' => 'This is a test task',
                 ]);
});
