<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\{
    Task,
    User
};

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id'      => User::factory(),
            'title'        => $this->faker->sentence,
            'description'  => $this->faker->paragraph,
            'completed_at' => $this->faker->optional()->dateTime,
        ];
    }

    /**
     * Indicate that the task is completed.
     *
     * @return Factory
     */
    public function completed(): Factory
    {
        return $this->state([
                                'completed_at' => now(),
                            ]);
    }

    /**
     * Indicate that the task is incomplete.
     *
     * @return Factory
     */
    public function incomplete(): Factory
    {
        return $this->state([
                                'completed_at' => null,
                            ]);
    }
}
