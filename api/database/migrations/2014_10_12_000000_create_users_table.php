<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            /**
             * User's unique identifier
             */
            $table->id();

            /**
             * User's name
             */
            $table->string('name');

            /**
             * User's unique email
             */
            $table
                ->string('email')
                ->unique();

            /**
             * Timestamp when the email is verified. Nullable if not verified.
             */
            $table
                ->timestamp('email_verified_at')
                ->nullable();

            /**
             * Hashed password for the user
             */
            $table->string('password');

            /**
             * Token for "remember me" functionality
             */
            $table->rememberToken();

            /**
             * Timestamps for when the user is created and last updated
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
