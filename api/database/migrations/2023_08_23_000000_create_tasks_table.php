<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Create tasks table to store tasks information.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            /**
             * Create primary key column
             */
            $table->id();

            /**
             * Owner of the task
             */
            $table->unsignedBigInteger('user_id');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            /**
             * Create column for task title
             */
            $table->string('title');

            /**
             * Create column for task description
             */
            $table->text('description');

            /**
             * Create column for task completion date
             */
            $table
                ->timestamp('completed_at')
                ->nullable();

            /**
             * Create column for soft delete
             */
            $table->softDeletes();

            /**
             * Create timestamp columns for created_at and updated_at
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * Drop tasks table.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
