<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     * Create attachments table to store file attachments.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('attachments', function (Blueprint $table) {
            /**
             * Create primary key column
             */
            $table->id();

            /**
             * Create foreign key column for associated task
             */
            $table
                ->unsignedBigInteger('task_id');

            $table
                ->foreign('task_id')
                ->references('id')
                ->on('tasks');

            /**
             * Create column for file path
             */
            $table->text('file_path');

            /**
             * Create timestamp columns for created_at and updated_at
             */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * Drop attachments table.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('attachments');
    }
};
