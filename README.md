# John Task List

## Story
João is tired of forgetting his tasks that he need to do daily. Your goal is to create a
tool that helps João to solve his problem in a simple way.

## API
The API should have the following endpoints:
- /tasks (GET): Returns the list of all tasks. 
- /tasks (POST): Creates a new task. 
- /tasks/{id} (GET): Returns the details of a specific task.
- /tasks/{id} (PUT): Updates the details of a specific task.
- /tasks/{id} (DELETE): Removes a specific task.

## Each task should have the following fields
- Unique identifier of the task.
- Task Title.
- Task Description.
- Attachment File(s).
- Availability of external APIs is not guaranteed and should not cause page to crash.
- Dates ( date that the task was created, completed, updated and deleted)
- User (that created the task or updated)

## Ensure proper error handling in the following cases
- The requested resource does not exist
- Validation errors in the data sent by the client

## To run the local dev environment:

### API
- Navigate to `/api` folder
- Ensure version docker installed is active on host
- Copy .env.example: `cp .env.example .env`
- Start docker containers `docker compose up` (add `-d` to run detached)
- Connect to container to run commands: `docker exec -it john-task-list-1 bash` or `winpty docker exec -it john-task-list-app-1 bash`
  - Make sure you are in the `/var/www/html` path
  - Install php dependencies: `composer install`
  - Setup app key: `php artisan key:generate`
  - Migrate database: `php artisan migrate` 
  - Seed database: `php artisan db:seed`
  - Run tests: `php artisan test`
- Visit api: `http://localhost`